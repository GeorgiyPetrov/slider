$(document).ready(function(){

  let values = {
    min: 400, // Это передвававть
    max: 7050
  };

  

  var startSlider = document.getElementById('slider-dot-start');
  var endSlider = document.getElementById('slider-dot-end');
  var track = document.getElementById('slider-line');
  var solidTrack = document.getElementById('slider-line-background');
  var solidTrackCords = solidTrack.getBoundingClientRect();

  let _1pxSliderScaleValue = (values.max - values.min) / solidTrackCords.width;

  let minBorder = 0;
  let maxBorder = solidTrackCords.width;
  console.log('ss1', solidTrackCords)

  var listenerStart = function(e) {
    e.preventDefault();
      var xCoord = e.pageX - solidTrackCords.left;

      if(xCoord < 0) xCoord = 0;
      if(xCoord > maxBorder) xCoord = maxBorder;

      startSlider.style.left = xCoord + "px";
      track.style.left = xCoord + "px";
      track.style.width = maxBorder-xCoord + "px"; 

      minBorder = xCoord;

      // установить значение в инпут
      let minInput = document.getElementById('minval');
      minInput.value = (values.min + xCoord * _1pxSliderScaleValue).toFixed(0);
    };
  
  var listenerEnd = function(e) {
    e.preventDefault();
      var xCoord = e.pageX - solidTrackCords.x;

      if(xCoord < minBorder) xCoord = minBorder;
      if(xCoord > solidTrackCords.width) 
      {
        xCoord = solidTrackCords.width;
      }
      endSlider.style.left = xCoord + "px";
      let trackWidth = e.pageX - (solidTrackCords.x + minBorder);
      if(e.pageX > solidTrackCords.x + solidTrackCords.width) {
        trackWidth = solidTrackCords.width - minBorder;
      }
      track.style.width = trackWidth + "px";

      maxBorder = xCoord;

      // установить значение в инпут
      let maxInput = document.getElementById('maxval');
      maxInput.value = (values.min + xCoord * _1pxSliderScaleValue).toFixed(0);
    };

  startSlider.addEventListener('mousedown', e => {
    document.addEventListener('mousemove', listenerStart);
  });

  endSlider.addEventListener('mousedown', e => {
    document.addEventListener('mousemove', listenerEnd);
  });

  document.addEventListener('mouseup', e => {
    document.removeEventListener('mousemove', listenerStart);
    document.removeEventListener('mousemove', listenerEnd);
  });

  var  minInput = document.getElementById('minval');
  minInput.onchange = function(){
    console.log('xx1', minInput.value);
  }

  var  maxInput = document.getElementById('maxval');
  maxInput.onchange = function(){
    console.log('xx2',maxInput.value);
  }
});